#!/bin/bash
set -e

sudo apt-get install git python python-pip libpcre3 libpcre3-dev \
	libpcap-dev libdumbnet-dev bison flex zlib1g-dev net-tools luajit

git clone git://github.com/osrg/ryu.git
cd ryu; pip install .

cd .. 
git clone git://github.com/mininet/mininet
cd mininet
git checkout -b 2.2.2
cd ..
mininet/util/install.sh -nfv

wget https://www.snort.org/downloads/snort/daq-2.0.6.tar.gz
wget https://www.snort.org/downloads/snort/snort-2.9.12.tar.gz

tar xzvf daq-2.0.6.tar.gz
cd daq-2.0.6
./configure && make && sudo make install
cd ..
tar xzvf snort-2.9.12.tar.gz
cd snort-2.9.12
./configure --enable-sourcefire --prefix=/usr/local --disable-open-appid && make && sudo make install

cd ..
cp snort.conf snort-2.9.12/etc/
mkdir  snort-2.9.12/rules
cp sdn.rules  snort-2.9.12/rules/
