# SDN + WAF

Celem projektu jest pokazanie możliwości połączenia Web Application Firewall (w skrócie WAF), firewalla aplikacyjnego z sieciami sterowanymi programowo (SDN, ang. Software defined Networks).

## Wykorzystane narzędzia

### Ryu

Sterownik SDN napisany w języku Python, z dobrze udokumentowanym API także w języku Python.

### Snort

Najbardziej popularny wśród aplikacyjnych WAFów, pozwalający na proste definiowanie reguł.

### Mininet

Emulator środowiska sieciowego. Pozwala na uruchomienie przełączników i urządzeń końcowych dzięki wirtualizacji procesów.

### Scapy

Generator pakietów z API w języku Python.

## Instalacja

### Zależności

* git
* pip
* libpcre3
* libpcre3-dev
* libpcap-dev
* libdumbnet-dev
* bison
* flex
* zlib1g-dev
* net-tools
* luajit - Ubuntu Bionic ma problemy z libluajit, można przy configure snorta dopisać `--disable-open-appid`

``` bash
sudo apt-get install git pip libpcre3 libpcre3-dev \
libpcap-dev libdumbnet-dev bison flex zlib1g-dev net-tools luajit
```

### Ryu

``` bash
git clone git://github.com/osrg/ryu.git
cd ryu; pip install .
```

### Mininet

``` bash
git clone git://github.com/mininet/mininet
cd mininet
git checkout -b 2.2.2
cd ..
mininet/util/install.sh -nfv
```

### Snort

``` bash
wget https://www.snort.org/downloads/snort/daq-2.0.6.tar.gz
wget https://www.snort.org/downloads/snort/snort-2.9.12.tar.gz

tar xvzf daq-2.0.6.tar.gz
cd daq-2.0.6
./configure && make && sudo make install
cd ..
tar xvzf snort-2.9.12.tar.gz
cd snort-2.9.12
./configure --enable-sourcefire --prefix=/usr/local --disable-open-appid && make && sudo make install
```

#### Konfiguracja snort

1) Skopiować snort.conf do `/home/$USER/snort-2.9.12/etc/`
2) w `/home/$USER/snort-2.9.12` stworzyć katalog *rules* i skopiować tam *sdn.rules*

## Uruchomienie

### Ryu

``` bash
cd ryu
bin/ryu-manager ~/SDN/simple_switch_snort.py
```

### Mininet

``` bash
sudo mn --topo single,2 --controller remote
```

### Snort

``` bash
sudo snort -i s1-eth1 -A unsock -l /tmp -c /home/$USER/snort-2.9.12/etc/snort.conf
```

### Scapy

``` bash
pip install scapy
```

## Ataki

Po uruchomieniu, możemy sprawdzić czy nasze reguły działają. Z linii poleceń programu mininet, wydajemy np.:
`h1 ~/SDN/attacks/synflood.py h2`
Możemy patrzeć, czy scapy nadal wysyła i odbiera pakiety, możemy także podejrzeć ruch sieciowy (a raczej jego brak) za pomocą wiresharka (najlepiej na interfejsie s1-eth2 - łączącym s1 z h2)

W katalogu ataki, znajdziemy skrypty wywołujące następujące ataki:
* SYN flood
* SQL injection (w różnym wydaniu)
* slow loris (a własciwie jedną z jego odmian)

Dodatkowo, pierwsza z reguł snorta wykryje także pingflood, który wystarczy wywołać odpowiednio zmienszając interwał wysyłania pakietów programu ping (-i)

## Todo

* **DONE** ~~Dodać obsługe pakietów (może być Alert) w simple_switch_snort.py~~
* **DONE** ~~wygenerować kilka ataków (ping-flood, coś aplikacyjnego typu SQLi)~~
* **DONE** ~~dodać rule które wykryją powyższe~~
* **DONE** ~~dla ambitnych: przerobić instalacje na skrypt bash lub playbook ansible (lub dowolny inny działający automat)~~

## Źródła

https://ryu.readthedocs.io/en/latest/snort_integrate.html